#![no_main]
// If you want to try std support, also update the guest Cargo.toml file
// #![no_std] // std support is experimental

use risc0_zkvm::guest::env;

risc0_zkvm::guest::entry!(main);

/*
fn fibo(n: u64) -> u64 {
    if (n <= 1) {
        n
    } else {
        fibo(n - 1) + fibo(n - 2)
    }
}
pub fn main() {
    // TODO: Implement your guest code here
    let a: u64 = env::read();
    let result = fibo(a);
    env::commit(&result);
}
*/

// fn fibo(n: u64, i: u64, a: u64, b: u64) -> u64 {
//     if i == n {
//         a
//     } else {
//         fibo(n, i + 1, b, a + b)
//     }
// }

// pub fn main() {
//     /*
//     TODO: Implement your guest code here
//     */
//     let a: u64 = env::read();

//     let result = fibo(a, 0, 0, 1);
//     env::commit(&result);
// }

pub fn bubble_sort(collection: &mut [u32]) {
    for _ in 0..collection.len() {
        let mut swaps = 0;
        for i in 1..collection.len() {
            if collection[i - 1] > collection[i] {
                collection.swap(i - 1, i);
                swaps += 1;
            }
        }
        if swaps == 0 {
            break;
        }
    }
}

pub fn main() {
    let mut a: Vec<u32> = env::read();
    bubble_sort(&mut a);
    env::commit(&true);
}
