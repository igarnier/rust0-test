// TODO: Update the name of the method loaded by the prover. E.g., if the method
// is `multiply`, replace `FIBONACCI_ELF` with `MULTIPLY_ELF` and replace
// `FIBONACCI_ID` with `MULTIPLY_ID`

use clap::Parser;
use fib_methods::{FIBONACCI_ELF, FIBONACCI_ID};
use risc0_zkvm::{
    default_executor_from_elf,
    serde::{from_slice, to_vec},
    ExecutorEnv,
};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short, long)]
    pub arg: u64,
}

fn main() {
    // TODO: add guest input to the executor environment using
    // ExecutorEnvBuilder::add_input().
    // To access this method, you'll need to use the alternate construction
    // ExecutorEnv::builder(), which creates an ExecutorEnvBuilder. When you're
    // done adding input, call ExecutorEnvBuilder::build().

    let args = Args::parse();

    let input: u32 = args.arg as u32;

    let to_sort: Vec<u32> = (0..input).rev().collect();
    // For example:
    let env = ExecutorEnv::builder().add_input(&to_sort).build().unwrap();

    use std::time::Instant;

    // Next, we make an executor, loading the (renamed) ELF binary.
    let mut exec = default_executor_from_elf(env, FIBONACCI_ELF).unwrap();

    // Run the executor to produce a session.
    let now = Instant::now();
    let session = exec.run().unwrap();
    let receipt = session.prove().unwrap();

    let session_time = now.elapsed().as_millis();
    println!("{input}, {session_time}");

    receipt.verify(FIBONACCI_ID).unwrap();

    let _output: bool = from_slice(&receipt.journal).unwrap();
}
